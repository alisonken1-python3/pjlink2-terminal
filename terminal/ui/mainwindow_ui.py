# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'resources/forms/main_window.ui'
#
# Created by: PyQt5 UI code generator 5.10
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1024, 800)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.interfaceSelect = QtWidgets.QComboBox(self.centralwidget)
        self.interfaceSelect.setGeometry(QtCore.QRect(330, 10, 220, 30))
        self.interfaceSelect.setObjectName("interfaceSelect")
        self.interfaceMultiple = QtWidgets.QCheckBox(self.centralwidget)
        self.interfaceMultiple.setGeometry(QtCore.QRect(10, 15, 181, 21))
        self.interfaceMultiple.setChecked(False)
        self.interfaceMultiple.setObjectName("interfaceMultiple")
        self.interfaceLabel = QtWidgets.QLabel(self.centralwidget)
        self.interfaceLabel.setGeometry(QtCore.QRect(245, 15, 64, 20))
        self.interfaceLabel.setObjectName("interfaceLabel")
        self.terminalStatusLabel = QtWidgets.QLabel(self.centralwidget)
        self.terminalStatusLabel.setGeometry(QtCore.QRect(600, 15, 81, 17))
        self.terminalStatusLabel.setObjectName("terminalStatusLabel")
        self.terminalStatus = QtWidgets.QLabel(self.centralwidget)
        self.terminalStatus.setGeometry(QtCore.QRect(697, 15, 81, 17))
        self.terminalStatus.setObjectName("terminalStatus")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1024, 29))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuHelp = QtWidgets.QMenu(self.menubar)
        self.menuHelp.setObjectName("menuHelp")
        self.menuSettings = QtWidgets.QMenu(self.menubar)
        self.menuSettings.setObjectName("menuSettings")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setAutoFillBackground(True)
        self.statusbar.setSizeGripEnabled(True)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionAbout = QtWidgets.QAction(MainWindow)
        self.actionAbout.setObjectName("actionAbout")
        self.actionQuit = QtWidgets.QAction(MainWindow)
        self.actionQuit.setObjectName("actionQuit")
        self.actionConfigure = QtWidgets.QAction(MainWindow)
        self.actionConfigure.setObjectName("actionConfigure")
        self.menuFile.addAction(self.actionQuit)
        self.menuHelp.addAction(self.actionAbout)
        self.menuSettings.addAction(self.actionConfigure)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuSettings.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "PJLink2 Terminal"))
        self.interfaceSelect.setToolTip(_translate("MainWindow", "Select server terminal to control"))
        self.interfaceMultiple.setToolTip(_translate("MainWindow", "When checked start a terminal on each interface separately"))
        self.interfaceMultiple.setText(_translate("MainWindow", "Multiple Terminals"))
        self.interfaceLabel.setText(_translate("MainWindow", "Interface:"))
        self.terminalStatusLabel.setText(_translate("MainWindow", "Power State:"))
        self.terminalStatus.setText(_translate("MainWindow", "Off"))
        self.menuFile.setTitle(_translate("MainWindow", "&File"))
        self.menuHelp.setTitle(_translate("MainWindow", "He&lp"))
        self.menuSettings.setTitle(_translate("MainWindow", "Setti&ngs"))
        self.actionAbout.setText(_translate("MainWindow", "&About"))
        self.actionQuit.setText(_translate("MainWindow", "&Quit"))
        self.actionConfigure.setText(_translate("MainWindow", "&Configure"))

