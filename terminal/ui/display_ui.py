
# -*- coding: utf-8 -*-
# vim: autoindent shiftwidth=4 expandtab textwidth=120 tabstop=4 softtabstop=4

###############################################################################
# PJLink2 Terminal - PJLink terminal emulator                                 #
# --------------------------------------------------------------------------- #
# Copyright (c) 2018 - Ken Roberts                                            #
# --------------------------------------------------------------------------- #
# This program is free software; you can redistribute it and/or modify it     #
# under the terms of the GNU General Public License as published by the Free  #
# Software Foundation; version 2 of the License.                              #
#                                                                             #
# This program is distributed in the hope that it will be useful, but WITHOUT #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for    #
# more details.                                                               #
#                                                                             #
# You should have received a copy of the GNU General Public License along     #
# with this program; if not, write to the Free Software Foundation, Inc., 59  #
# Temple Place, Suite 330, Boston, MA 02111-1307 USA                          #
###############################################################################
"""
The :mod:`display_ui` module contains the setup for the display windows

The individual blocks are built using QtDesigner - by keeping the actual building
code separate from the individual blocks we can update the sections using QtDesigner
without having to modify the generated code every time a visual object changes.
"""
import logging

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import pyqtSlot

from terminal.common import LOG_FORMAT, LOG_PROGRAM, LOG_NETWORK, LOG_NAME, get_logger, get_root_logger
from terminal.common.constants import IP_SET, status
from terminal.common.i18n import UiStrings
from terminal.common.settings import Settings
from terminal.core.pjlink2 import Pjl2Server
from terminal.ui.mainwindow_ui import Ui_MainWindow
from terminal.ui.tablog_ui import Ui_TabLog
from terminal.ui.tabsetup_ui import Ui_TabSetup
from terminal.ui.tabterminal_ui import Ui_TabTerminal


log = get_logger(__name__)

LOG_INDEX = {0: "CRITICAL",
             1: "FATAL",
             2: "ERROR",
             3: "WARNING",
             4: "INFO",
             5: "DEBUG",
             6: "NOTSET",
             "CRITICAL": 0,
             "FATAL": 1,
             "ERROR": 2,
             "WARNING": 3,
             "INFO": 4,
             "DEBUG": 5,
             "NOTSET": 6
             }

LOG_INDEX2LEVEL = {0: logging.CRITICAL,
                   1: logging.FATAL,
                   2: logging.ERROR,
                   3: logging.WARNING,
                   4: logging.INFO,
                   5: logging.DEBUG,
                   6: logging.NOTSET
                   }

LOG_LEVEL2INDEX = {logging.CRITICAL: 0,
                   logging.FATAL: 1,
                   logging.ERROR: 2,
                   logging.WARNING: 3,
                   logging.INFO: 4,
                   logging.DEBUG: 5,
                   logging.NOTSET: 6
                   }

ERST_STATUS = {0: UiStrings().Ok,
               1: UiStrings().Warning,
               2: UiStrings().Error
               }


class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    """
    Main window for the program
    """
    def __init__(self):
        """
        This constructor sets up the interface, the various managers, and the plugins.
        """
        log.debug('Initializing MainWindow()')
        super().__init__()
        self.terminals = []
        self.current_terminal = None
        self.setupUi(self)

        # We add the tab widget here so we can use separate files to be able to use the separate
        # tab widgets
        log.debug('Creating central tab widget')
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setGeometry(QtCore.QRect(10, 50, 1000, 700))
        self.tabWidget.setAutoFillBackground(True)
        self.tabWidget.setTabShape(QtWidgets.QTabWidget.Rounded)
        self.tabWidget.setObjectName("tabWidget")

        # Setup tab
        log.debug('Creating the "Setup" tab')
        self.tabSetup = TabSetup()
        self.tabSetup.setAutoFillBackground(True)
        self.tabSetup.setObjectName("tabSetup")
        self.tabWidget.addTab(self.tabSetup, "")

        # Terminal tab
        log.debug('Creating the "Terminal" tab')
        self.tabTerminal = TabTerminal()
        self.tabTerminal.setObjectName("tabTerminal")
        self.tabWidget.addTab(self.tabTerminal, "")

        # Log tab
        log.debug('Creating the "Log" tab')
        self.tabLog = TabLog()
        self.tabLog.setObjectName("tabLog")
        self.tabWidget.addTab(self.tabLog, "")

        self.tabWidget.setCurrentIndex(Settings().value('tabcentral/current index'))
        self.retranslateTabUi()
        log.debug('UI Initialized')

        log.debug('Setting up terminal')
        self.lamp_counter = self.tabSetup.findChild(QtWidgets.QSpinBox, 'lampCount')
        self.filter_counter = self.tabSetup.findChild(QtWidgets.QSpinBox, 'filterCount')
        self.interfaceSelect = self.findChild(QtWidgets.QComboBox, 'interfaceSelect')

        log.debug('Adding terminal(s)')
        for item in IP_SET:
            term = Pjl2Server(adx=IP_SET[item])
            self.interfaceSelect.addItem(term.name)
            for count in range(1, self.lamp_counter.value() + 1):
                item = 'lamp{number}'.format(number=count)
                term.add_lamp(item)
            for count in range(1, self.filter_counter.value() + 1):
                item = 'filter{number}'.format(number=count)
                term.add_filter(item)
            self.terminals.append(term)
        # Default to the first interface
        self.interfaceSelect.setCurrentIndex(0)
        self.current_terminal = self.terminals[0]

        self.current_terminal.power_state_callback = self.terminalStatus
        self.tabSetup.current_terminal = self.current_terminal
        self.tabSetup.connect_current_terminal()
        self.tabTerminal.current_terminal = self.current_terminal
        self.tabTerminal.connect_current_terminal()
        # Connect signals now that we have everything setup
        self.interfaceSelect.currentIndexChanged.connect(self.change_terminal)

    def retranslateTabUi(self):
        """
        And setup the retranslate for the tabs here
        """
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tabSetup), UiStrings().Setup)
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tabTerminal), UiStrings().Terminal)
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tabLog), UiStrings().Logs)

    @pyqtSlot(int)
    def change_terminal(self, pos):
        """
        Change terminal detected in interfaceSelect
        """
        term = self.terminals[pos]

        log.debug('Disconnecting display from terminal {name}'.format(name=self.current_terminal.name))
        self.tabTerminal.disconnect_current_terminal()
        self.tabTerminal.current_terminal = None
        self.tabSetup.disconnect_current_terminal()
        self.tabTerminal.current_terminal = None
        self.current_terminal.power_state_callback = None

        log.debug('Connecting display to terminal {name}'.format(name=term.name))
        self.current_terminal = term
        term.power_state_callback = self.terminalStatus
        self.terminalStatus.setText(term.power_state.short)
        self.tabTerminal.current_terminal = term
        self.tabTerminal.connect_current_terminal()
        self.tabSetup.current_terminal = term
        self.tabSetup.connect_current_terminal()

class TabLogger(logging.Handler):
    """
    Class to handle logging events to the tab log windows
    """
    def __init__(self, widget, window=LOG_PROGRAM):
        """
        Initialize the handler for the appropriate tab window

        :param widget: Widget that receives the log message
        :param window: Tab widget that receives log message
        """
        super().__init__()
        log.debug('Setting up handler for {name} tab'.format(name=LOG_NAME[window]))
        self.widget = widget
        self.setFormatter(LOG_FORMAT[window])
        self.name = LOG_NAME[window]

    def emit(self, record):
        msg = self.format(record)
        self.widget.appendPlainText(msg)


class TabSetup(QtWidgets.QWidget, Ui_TabSetup):
    """
    Setup tab for the program
    """
    def __init__(self):
        """
        This constructor builds the setup page of the main window tab
        """
        super().__init__()
        self.setupUi(self)
        self.current_terminal = None

    def connect_current_terminal(self):
        """
        Connects display objects to their individual PJLink terminal options on self.current_terminal
        """
        pass

    def disconnect_current_terminal(self):
        """
        Disconnects the displays from self.current_terminal
        """
        pass

    def set_current_terminal(self, term):
        """
        Go through the hooks for changing display to reflect current terminal
        """
        self.disconnect_current_terminal()
        self.current_terminal = term
        self.connect_current_terminal()


class TabTerminal(QtWidgets.QWidget, Ui_TabTerminal):
    """
    Terminal tab for the program
    """
    def __init__(self):
        """
        This constructor builds the terminal page of the main window tab
        """
        super().__init__()
        self.setupUi(self)
        self.current_terminal = None

        """
        for item in ['erstFan', 'erstLamp', 'erstCover', 'erstFilter', 'erstTemp', 'erstOther']:
            log.debug('Connecting {slider} slider to status label'.format(slider=item))
            slider = self.findChild(QtWidgets.QSlider, item)
            slider.valueChanged.connect(self.update_erst_status)
        """
        log.info('Connecting terminal tab signals to local methods')
        # Connect signals to local methods
        self.terminalPower.valueChanged.connect(self.update_terminal_power)
        self.erstCover.valueChanged.connect(self.update_erst_status)
        self.erstFan.valueChanged.connect(self.update_erst_status)
        self.erstFilter.valueChanged.connect(self.update_erst_status)
        self.erstLamp.valueChanged.connect(self.update_erst_status)
        self.erstOther.valueChanged.connect(self.update_erst_status)
        self.erstTemp.valueChanged.connect(self.update_erst_status)

    def connect_current_terminal(self):
        """
        Connects display objects to their individual PJLink terminal options on self.current_terminal
        """
        def updateItemCallback(item):
            log.debug('Updating callback for {item}'.format(item=item.name))
            txt = '{item}Status'.format(item=item.name)
            obj = self.findChild(QtWidgets.QLabel, txt)
            if obj is None:
                log.warning('{name} not found in tab'.format(name=txt))
                return
            item.power_callback = obj
            obj.setText(item.power_state.short)

        if self.current_terminal is None:
            log.warning('No terminal is currently connected to display - unable to connect')
            return
        log.info('Connecting displays to terminal {name}'.format(name=self.current_terminal.name))
        self.current_terminal.power_callback = self.terminalPower
        s = 0 if self.current_terminal.power_state is status.S_OFF else 1
        self.terminalPower.setSliderPosition(s)
        log.info('Connecting displays to terminal lamps')
        for item in self.current_terminal.lamps:
            updateItemCallback(item)
        log.info('Connecting displays to terminal filters')
        for item in self.current_terminal.filters:
            updateItemCallback(item)

    def disconnect_current_terminal(self):
        """
        Disconnects the displays from self.current_terminal
        """
        if self.current_terminal is None:
            log.warning('No terminal is currently connected to display - unable to disconnect')
            return
        log.info('Disconnecting displays from terminal {name}'.format(name=self.current_terminal.name))
        self.current_terminal.power_callback = None
        log.info('Disconnecting displays from terminal lamps')
        for item in self.current_terminal.lamps:
            item.power_callback = None
        log.info('Disconnecting displays from terminal filters')
        for item in self.current_terminal.filters:
            item.power_callback = None

    @pyqtSlot(int)
    def update_erst_status(self, pos):
        slider = self.sender()
        name = slider.objectName()
        text = ERST_STATUS[pos]
        label = '{name}Status'.format(name=name)
        log.debug('Updating status for {name} to {text}'.format(name=name, text=text))
        status = self.findChild(QtWidgets.QLabel, label)
        status.setText(text)

    @pyqtSlot(int)
    def update_terminal_power(self, pos):
        """
        Update terminals when power is applied (not pjlink.powr command)
        """
        if self.current_terminal is None:
            log.warning('No current terminal defined, unable to change terminal power state')
            return
        log.debug('Updating {name} power state'.format(name=self.current_terminal.name))
        # Relying on python's interpretation of 0 = False
        if pos:
            self.current_terminal.power_apply()
        else:
            self.current_terminal.power_remove()


class TabLog(QtWidgets.QWidget, Ui_TabLog):
    """
    Log tab for the program
    """
    def __init__(self):
        """
        This constructor builds the terminal page of the main window tab
        """
        super().__init__()
        self.setupUi(self)
        self.pgm_handler = TabLogger(widget=self.findChild(QtWidgets.QPlainTextEdit, 'programLog'),
                                     window=LOG_PROGRAM)
        log.debug('Adding handler to {log} logger'.format(log=LOG_NAME[LOG_PROGRAM]))
        root = get_root_logger(LOG_PROGRAM)
        root.addHandler(self.pgm_handler)
        root.setLevel(Settings().value('logprogram/log level'))
        log.debug('Adding handler to {log} logger'.format(log=LOG_NAME[LOG_NETWORK]))
        self.net_handler = TabLogger(widget=self.findChild(QtWidgets.QPlainTextEdit, 'networkLog'),
                                     window=LOG_NETWORK)
        root = get_root_logger(LOG_NETWORK)
        root.setLevel(Settings().value('lognetwork/log level'))
        root.addHandler(self.net_handler)

        log.debug('Connecting log level widgets to update log level')
        item = self.findChild(QtWidgets.QComboBox, 'programLogLevel')
        item.setCurrentIndex(LOG_LEVEL2INDEX[Settings().value('logprogram/log level')])
        item.currentIndexChanged.connect(self.update_log_level)

        item = self.findChild(QtWidgets.QComboBox, 'networkLogLevel')
        item.setCurrentIndex(LOG_LEVEL2INDEX[Settings().value('lognetwork/log level')])
        item.currentIndexChanged.connect(self.update_log_level)
        self.show()

    @pyqtSlot(int)
    def update_log_level(self, level):
        """
        Update the log level for the program log widget
        """
        widget = self.sender()
        name = widget.objectName()
        if name.lower().startswith('program'):
            logger = get_root_logger(LOG_PROGRAM)
            set_group = 'logrogram/log level'
        elif name.lower().startswith('network'):
            logger = get_root_logger(LOG_NETWORK)
            set_group = 'lognetwork/log level'
        else:
            log.warning('Widget {name} is not a log window - returning'.format(name=name))
            return
        log.debug('Updating {name} tab log level to {level}'.format(name=name, level=LOG_INDEX[level]))
        handler = None
        for handler in logger.handlers:
            if handler.get_name() in LOG_NAME:
                break
        if not handler:
            log.warning('No log handler found to update level: {level}'.format(level=level))
            return
        handler.setLevel(LOG_INDEX2LEVEL[level])
        Settings().setValue(set_group, LOG_INDEX2LEVEL[level])
        Settings().sync()
        self.show()
