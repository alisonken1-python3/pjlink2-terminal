# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'resources/forms/tab_log.ui'
#
# Created by: PyQt5 UI code generator 5.10
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_TabLog(object):
    def setupUi(self, TabLog):
        TabLog.setObjectName("TabLog")
        TabLog.resize(1000, 675)
        self.programLogLevelLabel = QtWidgets.QLabel(TabLog)
        self.programLogLevelLabel.setGeometry(QtCore.QRect(13, 53, 81, 17))
        self.programLogLevelLabel.setObjectName("programLogLevelLabel")
        self.networkLogLevelLabel = QtWidgets.QLabel(TabLog)
        self.networkLogLevelLabel.setGeometry(QtCore.QRect(13, 373, 81, 17))
        self.networkLogLevelLabel.setObjectName("networkLogLevelLabel")
        self.programLogLevel = QtWidgets.QComboBox(TabLog)
        self.programLogLevel.setGeometry(QtCore.QRect(13, 73, 95, 31))
        self.programLogLevel.setObjectName("programLogLevel")
        self.programLogLevel.addItem("")
        self.programLogLevel.addItem("")
        self.programLogLevel.addItem("")
        self.programLogLevel.addItem("")
        self.programLogLevel.addItem("")
        self.programLogLevel.addItem("")
        self.programLogLabel = QtWidgets.QLabel(TabLog)
        self.programLogLabel.setGeometry(QtCore.QRect(13, 13, 91, 17))
        self.programLogLabel.setObjectName("programLogLabel")
        self.networkLogLevel = QtWidgets.QComboBox(TabLog)
        self.networkLogLevel.setGeometry(QtCore.QRect(13, 393, 95, 31))
        self.networkLogLevel.setObjectName("networkLogLevel")
        self.networkLogLevel.addItem("")
        self.networkLogLevel.addItem("")
        self.networkLogLevel.addItem("")
        self.networkLogLevel.addItem("")
        self.networkLogLevel.addItem("")
        self.networkLogLevel.addItem("")
        self.networkLogLabel = QtWidgets.QLabel(TabLog)
        self.networkLogLabel.setGeometry(QtCore.QRect(13, 333, 91, 17))
        self.networkLogLabel.setObjectName("networkLogLabel")
        self.programLog = QtWidgets.QPlainTextEdit(TabLog)
        self.programLog.setGeometry(QtCore.QRect(123, 13, 860, 300))
        self.programLog.setReadOnly(True)
        self.programLog.setObjectName("programLog")
        self.networkLog = QtWidgets.QPlainTextEdit(TabLog)
        self.networkLog.setGeometry(QtCore.QRect(123, 333, 860, 300))
        self.networkLog.setReadOnly(True)
        self.networkLog.setObjectName("networkLog")

        self.retranslateUi(TabLog)
        self.programLogLevel.setCurrentIndex(3)
        self.networkLogLevel.setCurrentIndex(3)
        QtCore.QMetaObject.connectSlotsByName(TabLog)

    def retranslateUi(self, TabLog):
        _translate = QtCore.QCoreApplication.translate
        TabLog.setWindowTitle(_translate("TabLog", "Log"))
        self.programLogLevelLabel.setText(_translate("TabLog", "Log Level"))
        self.networkLogLevelLabel.setText(_translate("TabLog", "Log Level"))
        self.programLogLevel.setItemText(0, _translate("TabLog", "CRITICAL"))
        self.programLogLevel.setItemText(1, _translate("TabLog", "FATAL"))
        self.programLogLevel.setItemText(2, _translate("TabLog", "ERROR"))
        self.programLogLevel.setItemText(3, _translate("TabLog", "WARNING"))
        self.programLogLevel.setItemText(4, _translate("TabLog", "INFO"))
        self.programLogLevel.setItemText(5, _translate("TabLog", "DEBUG"))
        self.programLogLabel.setText(_translate("TabLog", "Program Log"))
        self.networkLogLevel.setItemText(0, _translate("TabLog", "CRITICAL"))
        self.networkLogLevel.setItemText(1, _translate("TabLog", "FATAL"))
        self.networkLogLevel.setItemText(2, _translate("TabLog", "ERROR"))
        self.networkLogLevel.setItemText(3, _translate("TabLog", "WARNING"))
        self.networkLogLevel.setItemText(4, _translate("TabLog", "INFO"))
        self.networkLogLevel.setItemText(5, _translate("TabLog", "DEBUG"))
        self.networkLogLabel.setText(_translate("TabLog", "Network Log"))

