# -*- coding: utf-8 -*-
# vim: autoindent shiftwidth=4 expandtab textwidth=120 tabstop=4 softtabstop=4

###############################################################################
# PJLink2 Terminal - PJLink terminal emulator                                 #
# --------------------------------------------------------------------------- #
# Copyright (c) 2018 - Ken Roberts                                            #
# --------------------------------------------------------------------------- #
# This program is free software; you can redistribute it and/or modify it     #
# under the terms of the GNU General Public License as published by the Free  #
# Software Foundation; version 2 of the License.                              #
#                                                                             #
# This program is distributed in the hope that it will be useful, but WITHOUT #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for    #
# more details.                                                               #
#                                                                             #
# You should have received a copy of the GNU General Public License along     #
# with this program; if not, write to the Free Software Foundation, Inc., 59  #
# Temple Place, Suite 330, Boston, MA 02111-1307 USA                          #
###############################################################################
"""
The :mod:`constants` module contains common variables
"""
import os
from enum import Enum, auto, unique

from PyQt5.QtNetwork import QAbstractSocket, QNetworkInterface

from terminal.common import IS_LINUX, translate
from terminal.common.i18n import UiStrings

X11_BYPASS_DEFAULT = True
if IS_LINUX:  # pragma: no cover
    # Default to False on Gnome.
    X11_BYPASS_DEFAULT = bool(not os.environ.get('GNOME_DESKTOP_SESSION_ID'))
    # Default to False on Xfce.
    if os.environ.get('DESKTOP_SESSION') == 'xfce':
        X11_BYPASS_DEFAULT = False

# Set PJLink constants.
CR = chr(0x0D)  # *nix \r
LF = chr(0x0A)  # *nix \n - not needed for strict PJLink but if using telnet may be needed
BUFFER = 140    # Default PJLink + 4
PJLINK_MAX_PACKET = 136  # Includes trailing CR
PJLINK_MAX_DATA = 128
PJLINK_PREFIX = '%'
PJLINK_PORT = 4352
PJLINK_SUFFIX = CR
PJLINK_TIMEOUT = 30.0


@unique
class status_class(Enum):
    """
    Defines the status class
    """
    STATUS = auto()
    ERROR = auto()
    QSOCKET_ERROR = auto()
    PJLINK_ERROR = auto()
    QSOCKET_STATE = auto()
    TERMINAL_STATE = auto()
    NETWORK_STATE = auto()


@unique
class status(Enum):
    """
    Complete list of status codes used in the program.

    Useful tidbits:
        status.name         String name (ex: status.E_GENERAL.name = "E_GENERAL")
        status.val          Integer value of the item
        status.subclass     status_class that this belongs to
        status.short        Short text description of the item
        status.full         Full description/help text for the item

    To add a new status code:
        <name> = (
                    val         - <int> preferrably unique
                    subclass    - status_class.CLASS that this belongs to
                    short       - <str> Short description - may even be one or two words
                    full        - <str> Full description that can be used in help text
                 )
    """
    # General error codes
    E_OK = (200,
            status_class.STATUS,
            UiStrings().Ok,
            translate('PJLink2.Constants', 'Everything is OK'))
    E_GENERAL = (201,
                 status_class.ERROR,
                 UiStrings().GeneralError,
                 translate('PJLink2.Constants', 'A general error encountered'))
    E_NOT_CONNECTED = (202,
                       status_class.ERROR,
                       translate('PJLink2.Constants', 'Not connected error'),
                       translate('PJLink2.Constants', 'Terminal is not connected'))
    # PJLink-specific error codes
    E_UNDEFINED = (203,
                   status_class.PJLINK_ERROR,
                   'ERR1',
                   translate('PJLink2.Constants', 'ERR1: Undefined command was received from the controller'))
    E_PARAMETER = (204,
                   status_class.PJLINK_ERROR,
                   'ERR2',
                   translate('PJLink2.Constants', 'ERR2: Invalid Parameter received from terminal'))
    E_UNAVAILABLE = (205,
                     status_class.PJLINK_ERROR,
                     'ERR3',
                     translate('PJLink2.Constants', 'ERR3: Projector is busy - try again later'))
    E_TERMINAL = (206,
                  status_class.PJLINK_ERROR,
                  'ERR4',
                  translate('PJLink2.Constants', 'ERR4: General terminal error - call a technician'))
    E_AUTHENTICATION = (207,
                        status_class.PJLINK_ERROR,
                        'ERRA',
                        translate('PJLink2.Constants', 'ERRA: Authentication error - PIN did not match'))
    E_PREFIX = (208,
                status_class.PJLINK_ERROR,
                translate('PJLink2.Constants', 'Invalid prefix'),
                translate('PJLink2.Constants', 'Received packet did not have the correct first character'))
    E_CLASS = (209,
               status_class.PJLINK_ERROR,
               translate('PJLink2.Constants', 'PJLink class mismatch'),
               translate('PJLink2.Constants', 'Terminal class/version does not match controller command'))
    E_INVALID = (210,
                 status_class.PJLINK_ERROR,
                 translate('PJLink2.Constants', 'Invalid data'),
                 translate('PJLink2.Constants', 'Data received is invalid'))
    E_WARN = (211,
              status_class.PJLINK_ERROR,
              UiStrings().Warning,
              translate('PJLink2.Constants', 'Warning condition detected'))
    E_ERROR = (212,
               status_class.PJLINK_ERROR,
               UiStrings().Error,
               translate('PJLink2.Constants', 'A error condition was detected'))
    E_FAN = (213,
             status_class.PJLINK_ERROR,
             UiStrings().Fan,
             translate('PJLink2.Constants', 'Terminal detected an error with one of the fans'))
    E_LAMP = (214,
              status_class.PJLINK_ERROR,
              UiStrings().Lamp,
              translate('PJLink2.Constants', 'Terminal detected an error with one of the lamps'))
    E_TEMP = (215,
              status_class.PJLINK_ERROR,
              UiStrings().Temp,
              translate('PJLink2.Constants', 'Terminal high temperature detected'))
    E_COVER = (216,
               status_class.PJLINK_ERROR,
               UiStrings().Cover,
               translate('PJLink2.Constants', 'Terminal detected a cover was open'))
    E_FILTER = (217,
                status_class.PJLINK_ERROR,
                UiStrings().Filter,
                translate('PJLink2.Constants', 'Time to check the filter(s)'))
    E_OTHER = (218,
               status_class.PJLINK_ERROR,
               UiStrings().Other,
               translate('PJLink2.Constants', 'Terminal has encountered some other error'))
    E_UNKNOWN = (219,
                 status_class.PJLINK_ERROR,
                 translate('PJLink2.Constants', 'Unknown error'),
                 translate('PJLink2.Constants', 'Unknown condiction detected'))
    # Remap Qt socket error codes to local error codes
    E_CONNECTION_REFUSED = (230,
                            status_class.QSOCKET_ERROR,
                            translate('PJLink2.Constants', 'Connection refused error'),
                            translate('PJLink2.Constants', 'The connection was refused or timed out'))
    E_REMOTE_HOST_CLOSED_CONNECTION = (231,
                                       status_class.QSOCKET_ERROR,
                                       translate('PJLink2.Constants', 'Remote host closed connection error'),
                                       translate('PJLink2.Constants', 'The remote host closed the connection'))
    E_HOST_NOT_FOUND = (232,
                        status_class.QSOCKET_ERROR,
                        translate('PJLink2.Constants', 'Host not found error'),
                        translate('PJLink2.Constants', 'Could not resolve the network name to a network address'))
    E_SOCKET_ACCESS = (233,
                       status_class.QSOCKET_ERROR,
                       translate('PJLink2.Constants', 'Cannot access socket error'),
                       translate('PJLink2.Constants',
                                 'The socket operation failed because the application '
                                 'lacked the required privileges'))
    E_SOCKET_RESOURCE = (234,
                         status_class.QSOCKET_ERROR,
                         translate('PJLink2.Constants', 'Socket resources unavailable error'),
                         translate('PJLink2.Constants',
                                   'The local system ran out of resources (e.g., too many sockets)'))
    E_SOCKET_TIMEOUT = (235,
                        status_class.QSOCKET_ERROR,
                        translate('PJLink2.Constants', 'Socket timeout error'),
                        translate('PJLink2.Constants', 'The socket operation timed out'))
    E_DATAGRAM_TOO_LARGE = (236,
                            status_class.QSOCKET_ERROR,
                            translate('PJLink2.Constants', 'Datagram too large error'),
                            translate('PJLink2.Constants', "The datagram was larger than the operating system's limit"))
    E_NETWORK = (237,
                 status_class.QSOCKET_ERROR,
                 translate('PJLink2.Constants', 'General network error'),
                 translate('PJLink2.Constants',
                           'An error occurred with the network (Possibly someone pulled the plug?)'))
    E_ADDRESS_IN_USE = (238,
                        status_class.QSOCKET_ERROR,
                        translate('PJLink2.Constants', 'Address already in use error'),
                        translate('PJLink2.Constants',
                                  'The address specified with socket.bind() is already in use and was '
                                  'set to be exclusive'))
    E_SOCKET_ADDRESS_NOT_AVAILABLE = (239,
                                      status_class.QSOCKET_ERROR,
                                      translate('PJLink2.Constants', 'Socket address not available error'),
                                      translate('PJLink2.Constants',
                                                'The address specified to socket.bind() does not belong to the host'))
    E_UNSUPPORTED_SOCKET_OPERATION = (240,
                                      status_class.QSOCKET_ERROR,
                                      translate('PJLink2.Constants', 'Unsupported socket operation error'),
                                      translate('PJLink2.Constants',
                                                'The requested socket operation is not supported by the local '
                                                'operating system (e.g., lack of IPv6 support)'))
    E_PROXY_AUTHENTICATION_REQUIRED = (241,
                                       status_class.QSOCKET_ERROR,
                                       translate('PJLink2.Constants', 'Proxy error - authentication required'),
                                       translate('PJLink2.Constants',
                                                 'The socket is using a proxy, and the proxy requires authentication'))
    E_SLS_HANDSHAKE_FAILED = (242,
                              status_class.QSOCKET_ERROR,
                              translate('PJLink2.Constants', 'SLS error - handshake failed'),
                              translate('PJLink2.Constants', 'The SSL/TLS handshake failed'))
    E_UNFINISHED_SOCKET_OPERATION = (243,
                                     status_class.QSOCKET_ERROR,
                                     translate('PJLink2.Constants', 'Socket error - unfinished operation'),
                                     translate('PJLink2.Constants',
                                               'The last operation attempted has not finished yet '
                                               '(still in progress in the background)'))
    E_PROXY_CONNECTION_REFUSED = (244,
                                  status_class.QSOCKET_ERROR,
                                  translate('PJLink2.Constants', 'Proxy error - connection refused'),
                                  translate('PJLink2.Constants',
                                            'Could not contact the proxy server because the connection '
                                            'to that server was denied'))
    E_PROXY_CONNECTION_CLOSED = (245,
                                 status_class.QSOCKET_ERROR,
                                 translate('PJLink2.Constants', 'Proxy error - connection closed'),
                                 translate('PJLink2.Constants',
                                           'The connection to the proxy server was closed unexpectedly '
                                           '(before the connection to the final peer was established)'))
    E_PROXY_CONNECTION_TIMEOUT = (246,
                                  status_class.QSOCKET_ERROR,
                                  translate('PJLink2.Constants', 'Proxy error - connection timed out'),
                                  translate('PJLink2.Constants',
                                            'The connection to the proxy server timed out or the proxy '
                                            'server stopped responding in the authentication phase.'))
    E_PROXY_NOT_FOUND = (247,
                         status_class.QSOCKET_ERROR,
                         translate('PJLink2.Constants', 'Proxy error - proxy not found'),
                         translate('PJLink2.Constants',
                                   'The proxy address set with setProxy() was not found'))
    E_PROXY_PROTOCOL = (248,
                        status_class.QSOCKET_ERROR,
                        translate('PJLink2.Constants', 'Proxy error - protocol error'),
                        translate('PJLink2.Constants',
                                  'The connection negotiation with the proxy server failed because the '
                                  'response from the proxy server could not be understood'))
    E_UNKNOWN_SOCKET_ERROR = (249,
                              status_class.QSOCKET_ERROR,
                              translate('PJLink2.Constants', 'Socket error - unknown'),
                              translate('PJLink2.Constants', 'An unknown socket error occurred'))
    # General status codes
    S_OK = (300,
            status_class.STATUS,
            UiStrings().Ok,
            translate('PJLink2.Constants', 'Everything is OK'))
    # Remap Qt socket states to local status codes
    S_NOT_CONNECTED = (301,
                       status_class.QSOCKET_STATE,
                       translate('PJLink2.Constants', 'Not connected'),
                       translate('PJLink2.Constants', 'Terminal is not connected to a controller'))
    S_HOST_LOOKUP = (302,
                     status_class.QSOCKET_STATE,
                     translate('PJLink2.Constants', 'Host lookup'),
                     translate('PJLink2.Constants', 'Performing a host name lookup'))
    S_CONNECTING = (303,
                    status_class.QSOCKET_STATE,
                    UiStrings().Connecting,
                    translate('PJLink2.Constants', 'Attempting to make a network connection'))
    S_CONNECTED = (304,
                   status_class.QSOCKET_STATE,
                   UiStrings().Connected,
                   translate('PJLink2.Constants', 'Terminal is connected to a controller'))
    S_BOUND = (305,
               status_class.QSOCKET_STATE,
               UiStrings().Bound,
               translate('PJLink2.Constants', 'Socket is bound to an address and/or port'))
    # S_LISTENING Listed as internal use only in QAbstractSocket
    S_LISTENING = (306,
                   status_class.QSOCKET_STATE,
                   UiStrings().Listening,
                   translate('PJLink2.Constants', 'Socket is listening for a new connection'))
    S_CLOSING = (307,
                 status_class.QSOCKET_STATE,
                 UiStrings().Closing,
                 translate('PJLink2.Constants', 'Socket is closing and freeing resources'))
    # Terminal states
    S_INITIALIZE = (308,
                    status_class.TERMINAL_STATE,
                    UiStrings().Initializing,
                    translate('PJLink2.Constants', 'Initialization in progress'))
    S_OFF = (309,
             status_class.TERMINAL_STATE,
             UiStrings().Off,
             translate('PJLink2.Constants', 'Terminal is in the power off state'))
    S_STANDBY = (310,
                 status_class.TERMINAL_STATE,
                 UiStrings().Standby,
                 translate('PJLink2.Constants', 'Terminal is in the power standby state'))
    S_WARMUP = (311,
                status_class.TERMINAL_STATE,
                UiStrings().Warmup,
                translate('PJLink2.Constants', 'Terminal is in the warmup state'))
    S_ON = (312,
            status_class.TERMINAL_STATE,
            UiStrings().On,
            translate('PJLink2.Constants', 'Terminal is in the power on state'))
    S_COOLDOWN = (313,
                  status_class.TERMINAL_STATE,
                  UiStrings().Cooldown,
                  translate('PJLink2.Constants', 'Terminal is in the cooldown state'))
    # Information that does not affect status
    S_NETWORK_IDLE = (400,
                      status_class.NETWORK_STATE,
                      UiStrings().Idle,
                      translate('PJLink2.Constants', 'No network activity at this time'))
    S_NETWORK_SENDING = (401,
                         status_class.NETWORK_STATE,
                         UiStrings().Sending,
                         translate('PJLink2.Constants', 'Terminal is sending data out to the network'))
    S_NETWORK_RECEIVING = (402,
                           status_class.NETWORK_STATE,
                           UiStrings().Receiving,
                           translate('PJLink2.Constants', 'Terminal is receiving data from the network'))

    def __init__(self, val, subclass, short, full):
        """
        Builder to help create the enum with appropriate attributes
        val = int
        subclass = status_class this item belongs to
        short = short description
        long = long description - suitable for displaying a help message
        """
        self.val = val
        self.subclass = subclass
        self.short = short
        self.full = full


# Consolidate terminal power state
TERMINAL_STATE = [status.S_INITIALIZE,
                  status.S_OFF,
                  status.S_STANDBY,
                  status.S_WARMUP,
                  status.S_ON,
                  status.S_COOLDOWN
                  ]


# Map POWR return codes to status code
POWER_STATUS = [status.S_STANDBY,
                status.S_ON,
                status.S_COOLDOWN,
                status.S_WARMUP
                ]


# Map QAbstractSocketState enums to local status
# List position is QAbstractSocketState value
QSOCKET_STATE = [status.S_NOT_CONNECTED,     # 'UnconnectedState'
                 status.S_HOST_LOOKUP,       # 'HostLookupState',
                 status.S_CONNECTING,        # 'ConnectingState',
                 status.S_CONNECTED,         # 'ConnectedState',
                 status.S_BOUND,             # 'BoundState',
                 status.S_LISTENING,         # 'ListeningState' -  Noted as "Internal Use Only" on Qt website
                 status.S_CLOSING            # 'ClosingState'
                 ]


# Map ERST types
ERST_LIST = [status.E_FAN,
             status.E_LAMP,
             status.E_TEMP,
             status.E_COVER,
             status.E_FILTER,
             status.E_OTHER
             ]


# Map ERST errors
ERST_STATUS = [status.S_OK,
               status.E_WARN,
               status.E_ERROR
               ]


# NOTE: Changed format to account for some commands are both class 1 and 2.
#       Make sure the sequence of 'version' is lowest-to-highest.
PJLINK_VALID_CMD = {
    'ACKN': {'version': ['2'],
             'default': '2',
             'description': translate('PJLink2.PJLinkConstants',
                                      'Acknowledge a PJLink SRCH command - returns MAC address.')
             },
    'AVMT': {'version': ['1'],
             'default': '1',
             'description': translate('PJLink2.PJLinkConstants',
                                      'Blank/unblank video and/or mute audio.')
             },
    'CLSS': {'version': ['1'],
             'default': '1',
             'description': translate('PJLink2.PJLinkConstants',
                                      'Query projector PJLink class support.')
             },
    'ERST': {'version': ['1', '2'],
             'default': '1',
             'description': translate('PJLink2.PJLinkConstants',
                                      'Query error status from projector. '
                                      'Returns fan/lamp/temp/cover/filter/other error status.')
             },
    'FILT': {'version': ['2'],
             'default': '1',
             'description': translate('PJLink2.PJLinkConstants',
                                      'Query number of hours on filter.')
             },
    'FREZ': {'version': ['2'],
             'default': '1',
             'description': translate('PJLink2.PJLinkConstants',
                                      'Freeze or unfreeze current image being projected.')
             },
    'INF1': {'version': ['1'],
             'default': '1',
             'description': translate('PJLink2.PJLinkConstants',
                                      'Query projector manufacturer name.')
             },
    'INF2': {'version': ['1'],
             'default': '1',
             'description': translate('PJLink2.PJLinkConstants',
                                      'Query projector product name.')
             },
    'INFO': {'version': ['1'],
             'default': '1',
             'description': translate('PJLink2.PJLinkConstants',
                                      'Query projector for other information set by manufacturer.')
             },
    'INNM': {'version': ['2'],
             'default': '2',
             'description': translate('PJLink2.PJLinkConstants',
                                      'Query specified input source name')
             },
    'INPT': {'version': ['1'],
             'default': '1',
             'description': translate('PJLink2.PJLinkConstants',
                                      'Switch to specified video source.')
             },
    'INST': {'version': ['1'],
             'default': '1',
             'description': translate('PJLink2.PJLinkConstants',
                                      'Query available input sources.')
             },
    'IRES': {'version': ['2'],
             'default': '2',
             'description': translate('PJLink2.PJLinkConstants',
                                      'Query current input resolution.')
             },
    'LAMP': {'version': ['1'],
             'default': '1',
             'description': translate('PJLink2.PJLinkConstants',
                                      'Query lamp time and on/off status. Multiple lamps supported.')
             },
    'LKUP': {'version': ['2'],
             'default': '2',
             'description': translate('PJLink2.PJLinkConstants',
                                      'UDP Status - Projector is now available on network. Includes MAC address.')
             },
    'MVOL': {'version': ['2'],
             'default': '1',
             'description': translate('PJLink2.PJLinkConstants',
                                      'Adjust microphone volume by 1 step.')
             },
    'NAME': {'version': ['1'],
             'default': '1',
             'description': translate('PJLink2.PJLinkConstants',
                                      'Query customer-set projector name.')
             },
    'PJLINK': {'version': ['1'],
               'default': '1',
               'description': translate('PJLink2.PJLinkConstants',
                                        'Initial connection with authentication/no authentication request.')
               },
    'POWR': {'version': ['1'],
             'default': '1',
             'description': translate('PJLink2.PJLinkConstants',
                                      'Turn lamp on or off/standby.')
             },
    'RFIL': {'version': ['2'],
             'default': '2',
             'description': translate('PJLink2.PJLinkConstants',
                                      'Query replacement air filter model number.')
             },
    'RLMP': {'version': ['2'],
             'default': '2',
             'description': translate('PJLink2.PJLinkConstants',
                                      'Query replacement lamp model number.')
             },
    'RRES': {'version': ['2'],
             'default': '2',
             'description': translate('PJLink2.PJLinkConstants',
                                      'Query recommended resolution.')
             },
    'SNUM': {'version': ['2'],
             'default': '2',
             'description': translate('PJLink2.PJLinkConstants',
                                      'Query projector serial number.')
             },
    'SRCH': {'version': ['2'],
             'default': '2',
             'description': translate('PJLink2.PJLinkConstants',
                                      'UDP broadcast search request for available projectors. Reply is ACKN.')
             },
    'SVER': {'version': ['2'],
             'default': '2',
             'description': translate('PJLink2.PJLinkConstants',
                                      'Query projector software version number.')
             },
    'SVOL': {'version': ['2'],
             'default': '2',
             'description': translate('PJLink2.PJLinkConstants',
                                      'Adjust speaker volume by 1 step.')
             }
}


PJLINK_DEFAULT_SOURCES = {
    '1': translate('PJLink2.SourceType', 'RGB'),
    '2': translate('PJLink2.SourceType', 'Video'),
    '3': translate('PJLink2.SourceType', 'Digital'),
    '4': translate('PJLink2.SourceType', 'Storage'),
    '5': translate('PJLink2.SourceType', 'Network'),
    '6': translate('PJLink2.SourceType', 'Internal')
}


PJLINK_DEFAULT_ITEMS = {
    '1': translate('PJLink2.SourceInput', '1'),
    '2': translate('PJLink2.SourceInput', '2'),
    '3': translate('PJLink2.SourceInput', '3'),
    '4': translate('PJLink2.SourceInput', '4'),
    '5': translate('PJLink2.SourceInput', '5'),
    '6': translate('PJLink2.SourceInput', '6'),
    '7': translate('PJLink2.SourceInput', '7'),
    '8': translate('PJLink2.SourceInput', '8'),
    '9': translate('PJLink2.SourceInput', '9'),
    'A': translate('PJLink2.SourceInput', 'A'),
    'B': translate('PJLink2.SourceInput', 'B'),
    'C': translate('PJLink2.SourceInput', 'C'),
    'D': translate('PJLink2.SourceInput', 'D'),
    'E': translate('PJLink2.SourceInput', 'E'),
    'F': translate('PJLink2.SourceInput', 'F'),
    'G': translate('PJLink2.SourceInput', 'G'),
    'H': translate('PJLink2.SourceInput', 'H'),
    'I': translate('PJLink2.SourceInput', 'I'),
    'J': translate('PJLink2.SourceInput', 'J'),
    'K': translate('PJLink2.SourceInput', 'K'),
    'L': translate('PJLink2.SourceInput', 'L'),
    'M': translate('PJLink2.SourceInput', 'M'),
    'N': translate('PJLink2.SourceInput', 'N'),
    'O': translate('PJLink2.SourceInput', 'O'),
    'P': translate('PJLink2.SourceInput', 'P'),
    'Q': translate('PJLink2.SourceInput', 'Q'),
    'R': translate('PJLink2.SourceInput', 'R'),
    'S': translate('PJLink2.SourceInput', 'S'),
    'T': translate('PJLink2.SourceInput', 'T'),
    'U': translate('PJLink2.SourceInput', 'U'),
    'V': translate('PJLink2.SourceInput', 'V'),
    'W': translate('PJLink2.SourceInput', 'W'),
    'X': translate('PJLink2.SourceInput', 'X'),
    'Y': translate('PJLink2.SourceInput', 'Y'),
    'Z': translate('PJLink2.SourceInput', 'Z')
}


# Due to the expanded nature of PJLink class 2 video sources,
# translate the individual types then build the video source
# dictionary from the translations.
PJLINK_DEFAULT_CODES = dict()
for source in PJLINK_DEFAULT_SOURCES:
    for item in PJLINK_DEFAULT_ITEMS:
        label = "{source}{item}".format(source=source, item=item)
        PJLINK_DEFAULT_CODES[label] = "{source} {item}".format(source=PJLINK_DEFAULT_SOURCES[source],
                                                               item=PJLINK_DEFAULT_ITEMS[item])


'''
Setup the interface and IP definitions.

IP_SET = {"lo": {"hwadx": "00.00.00.00.00.00",
                 "ip4": QHostAddress(),
                 "ip4_adx" = "127.0.0.1",
                 "ip6": QHostAddress(),
                 "ip6_adx": "::1"
                }
          "eth0": {"hwadx": "xx:xx:xx:xx:xx:xx",
                   "ip4": QHostAddress(),
                   "ip4_adx" = "192.168.1.1",
                   "ip6": QHostAddress(),
                   "ip6_adx": "fe80::xxxx:xxxx:xxxx:xxxx%eth0"
         }
'''


IP_SET = {}
for iface in QNetworkInterface.allInterfaces():
    name = iface.name()
    chk = {"hwadx": iface.hardwareAddress()}
    for adx in iface.addressEntries():
        ip = adx.ip()
        if ip.protocol() == QAbstractSocket.IPv4Protocol:
            chk['ip4'] = ip
            chk['ip4_adx'] = ip.toString()
        if ip.protocol() == QAbstractSocket.IPv6Protocol:
            chk['ip6'] = ip
            chk['ip6_adx'] = ip.toString()
    IP_SET[name] = chk


# Cleanup namespace
del(iface, name, chk, adx, ip)
