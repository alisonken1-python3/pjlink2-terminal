# -*- coding: utf-8 -*-
# vim: autoindent shiftwidth=4 expandtab textwidth=120 tabstop=4 softtabstop=4

###############################################################################
# PJLink2 Terminal - PJLink terminal emulator                                 #
# --------------------------------------------------------------------------- #
# Copyright (c) 2018 - Ken Roberts                                            #
# --------------------------------------------------------------------------- #
# This program is free software; you can redistribute it and/or modify it     #
# under the terms of the GNU General Public License as published by the Free  #
# Software Foundation; version 2 of the License.                              #
#                                                                             #
# This program is distributed in the hope that it will be useful, but WITHOUT #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for    #
# more details.                                                               #
#                                                                             #
# You should have received a copy of the GNU General Public License along     #
# with this program; if not, write to the Free Software Foundation, Inc., 59  #
# Temple Place, Suite 330, Boston, MA 02111-1307 USA                          #
###############################################################################
"""
The :mod:`process_commands` module contains the code for processing replies
"""
from terminal.common import get_logger
from terminal.common.constants import PJLINK_PREFIX, PJLINK_SUFFIX

__all__ = ['reply']

log = get_logger(__name__)

# Format string for standard reply
# Data to be filled in by processor: version, cmd, data
STD_RESPONSE = '{prefix}{{version}}{{cmd}}={{data}){suffix}'.format(prefix=PJLINK_PREFIX,
                                                                    suffix=PJLINK_SUFFIX)


def process_erra(cmd, version=1):
    """
    Process ERRA: Authentication error
    """
    return STD_RESPONSE.format(cmd=cmd.upper(), version=version, data='ERRA')


def process_err1(cmd, version=1):
    """
    Process ERR1: Unidentified command
    """
    return STD_RESPONSE.format(cmd=cmd.upper(), version=version, data='ERR1')


def process_err2(cmd, version=1):
    """
    Process ERR2: Out of parameter
    """
    return STD_RESPONSE.format(cmd=cmd.upper(), version=version, data='ERR2')


def process_err3(cmd, version=1):
    """
    Process ERR3: Unavailable time
    """
    return STD_RESPONSE.format(cmd=cmd.upper(), version=version, data='ERR3')


def process_err4(cmd, version=1):
    """
    Process ERR4: Projector/display failure
    """
    return STD_RESPONSE.format(cmd=cmd.upper(), version=version, data='ERR4')


def process_ackn(version=2, *args, **kwargs):
    """
    NOTE: Terminal should never see this command.
    """
    return process_err1(cmd='ACKN', version=version)


def process_avmt(terminal, version=1):
    pass


def process_clss(version=1, *args, **kwargs):
    pass


def process_erst(terminal, version=1):
    pass


def process_filt(terminal, version=2):
    pass


def process_frez(terminal, version=2):
    pass


def process_inf1(terminal, version=1):
    pass


def process_inf2(terminal, version=1):
    pass


def process_info(terminal, version=1):
    pass


def process_innm(terminal, version=2):
    pass


def process_inpt(terminal, version=1):
    pass


def process_inst(terminal, version=1):
    pass


def process_ires(terminal, version=2):
    pass


def process_lamp(terminal, version=1):
    pass


def process_lkup(terminal, version=2):
    pass


def process_mvol(terminal, version=2):
    pass


def process_name(terminal, version=1):
    pass


def process_ok(cmd, version=1, *args, **kwargs):
    """
    Build OK reply

    :param cmd: Command initiating reply
    :returns: String
    """
    return STD_RESPONSE.format(cmd=cmd.upper(), version=version, data='OK')


def process_pjlink(version=1, *args, **kwargs):
    """
    Terminal should never see this command
    """
    return process_err1(cmd='PJLINK', version=version)


def process_powr(terminal, version=1):
    pass


def process_rfil(terminal, version=2):
    pass


def process_rlmp(terminal, version=2):
    pass


def process_rres(terminal, version=2):
    pass


def process_snum(terminal, version=2):
    pass


def process_srch(terminal, version=2):
    pass


def process_sver(terminal, version=2):
    pass


def process_svol(terminal, version=2):
    pass


# Map pjlink command to function
reply = {'EPASS': process_erra,  # Helper bad password
         'EBUSY': process_err3,  # Helper terminal busy
         'EBROKEN': process_err4,  # Helper general terminal failure
         'EPARAM': process_err2,  # Helper bad parameter
         'EUNK': process_err1,  # Helper unknown command
         'ERRA': process_erra,
         'ERR1': process_err1,
         'ERR2': process_err2,
         'ERR3': process_err3,
         'ERR4': process_err4,
         'ACKN': process_ackn,
         'AVMT': process_avmt,
         'CLSS': process_clss,
         'ERST': process_erst,
         'FILT': process_filt,
         'FREZ': process_frez,
         'INF1': process_inf1,
         'INF2': process_inf2,
         'INFO': process_info,
         'INNM': process_innm,
         'INPT': process_inpt,
         'INST': process_inst,
         'IRES': process_ires,
         'LAMP': process_lamp,
         'LKUP': process_lkup,
         'MVOL': process_mvol,
         'NAME': process_name,
         'OK': process_ok,
         'PJLINK': process_pjlink,
         'POWR': process_powr,
         'RFIL': process_rfil,
         'RLMP': process_rlmp,
         'RRES': process_rres,
         'SNUM': process_snum,
         'SRCH': process_srch,
         'SVOL': process_svol
         }
