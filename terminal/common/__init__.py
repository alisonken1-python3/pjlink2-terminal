# -*- coding: utf-8 -*-
# vim: autoindent shiftwidth=4 expandtab textwidth=120 tabstop=4 softtabstop=4

###############################################################################
# PJLink2 Terminal - PJLink terminal emulator                                 #
# --------------------------------------------------------------------------- #
# Copyright (c) 2018 - Ken Roberts                                            #
# --------------------------------------------------------------------------- #
# This program is free software; you can redistribute it and/or modify it     #
# under the terms of the GNU General Public License as published by the Free  #
# Software Foundation; version 2 of the License.                              #
#                                                                             #
# This program is distributed in the hope that it will be useful, but WITHOUT #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for    #
# more details.                                                               #
#                                                                             #
# You should have received a copy of the GNU General Public License along     #
# with this program; if not, write to the Free Software Foundation, Inc., 59  #
# Temple Place, Suite 330, Boston, MA 02111-1307 USA                          #
###############################################################################
"""
The :mod:`common` module contains all the common functionality
"""
import logging
import os
import sys

from PyQt5 import QtCore


# Due to dependency issues, this HAS to be at the top of the module
def translate(context, text, comment=None, qt_translate=QtCore.QCoreApplication.translate):
    """
    A special shortcut method to wrap around the Qt5 translation functions. This abstracts the translation procedure so
    that we can change it if at a later date if necessary, without having to redo the whole of PJLink2.

    :param context: The translation context, used to give each string a context or a namespace.
    :param text: The text to put into the translation tables for translation.
    :param comment: An identifying string for when the same text is used in different roles within the same context.
    :param qt_translate:
    """
    return qt_translate(context, text, comment)


file_formatter = logging.Formatter('%(asctime)s %(name)-55s %(levelname)-8s %(message)s')
tab_formatter = logging.Formatter('%(asctime)s %(funcName)-45s %(levelname)-8s %(message)s')

# Constants to define which of the main platforms we are running on
IS_LINUX = sys.platform.startswith('linux')
IS_MACOSX = sys.platform.startswith('darwin')
IS_WIN = os.name.startswith('nt')

X11_BYPASS_DEFAULT = True
if IS_LINUX:                                                                              # pragma: no cover
    # Default to False on Gnome.
    X11_BYPASS_DEFAULT = bool(not os.environ.get('GNOME_DESKTOP_SESSION_ID'))
    # Default to False on Xfce.
    if os.environ.get('DESKTOP_SESSION') == 'xfce':
        X11_BYPASS_DEFAULT = False


LOG_ROOT = 0
LOG_PROGRAM = 1
LOG_NETWORK = 2

LOG_PREFIX = {LOG_ROOT: 'root',
              LOG_PROGRAM: 'PGM',
              LOG_NETWORK: 'NET',
              'root': LOG_ROOT,
              'PGM': LOG_PROGRAM,
              'NET': LOG_NETWORK
              }

LOG_NAME = {LOG_ROOT: 'Root',
            LOG_PROGRAM: 'Program',
            LOG_NETWORK: 'Network',
            'Root': LOG_ROOT,
            'Program': LOG_PROGRAM,
            'Network': LOG_NETWORK
            }

ROOT_LOGGER = {LOG_ROOT: logging.getLogger(),
               LOG_PROGRAM: logging.getLogger(LOG_PREFIX[LOG_PROGRAM]),
               LOG_NETWORK: logging.getLogger(LOG_PREFIX[LOG_NETWORK])
               }

LOG_FORMAT = {LOG_ROOT: file_formatter,
              LOG_PROGRAM: tab_formatter,
              LOG_NETWORK: tab_formatter
              }


def get_root_logger(logtab=LOG_PROGRAM):
    """
    Return the root logger of the tab loggers

    :param window: Log type (LOG_PROGRAM or LOG_NETWORK)

    :returns: root logger instance
    """
    if ROOT_LOGGER[logtab] is not None:
        return ROOT_LOGGER[logtab]
    logger = logging.getLogger(LOG_PREFIX[logtab])
    logger.propagate = True
    ROOT_LOGGER[logtab] = logger
    return logger


def get_logger(name, logtab=LOG_PROGRAM, level=logging.INFO):
    """
    Returns a logger

    :param name: Logger name
    :param tab: Optional log path (default LOG_PROGRAM)
    :param level: Optional initial log level for this logger (default INFO)

    :returns: Logger instance
    """
    return logging.getLogger(LOG_PREFIX[logtab] + '.' + name)
