# -*- coding: utf-8 -*-
# vim: autoindent shiftwidth=4 expandtab textwidth=120 tabstop=4 softtabstop=4

###############################################################################
# PJLink2 Terminal - PJLink terminal emulator                                 #
# --------------------------------------------------------------------------- #
# Copyright (c) 2018 - Ken Roberts                                            #
# --------------------------------------------------------------------------- #
# This program is free software; you can redistribute it and/or modify it     #
# under the terms of the GNU General Public License as published by the Free  #
# Software Foundation; version 2 of the License.                              #
#                                                                             #
# This program is distributed in the hope that it will be useful, but WITHOUT #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for    #
# more details.                                                               #
#                                                                             #
# You should have received a copy of the GNU General Public License along     #
# with this program; if not, write to the Free Software Foundation, Inc., 59  #
# Temple Place, Suite 330, Boston, MA 02111-1307 USA                          #
###############################################################################
"""
The :mod:`terminal.common.settings` module keeps track of program settings and defaults
"""

import json
import logging

from PyQt5 import QtCore, QtGui

from terminal.common import X11_BYPASS_DEFAULT, get_logger
from terminal.common.json import PJLinkJsonDecoder, PJLinkJsonEncoder
from terminal.common.path import Path

__version__ = 0

log = get_logger(__name__)


class Settings(QtCore.QSettings):
    """
    Class to wrap QSettings.

    ``__default_settings__``
        This dict contains all core settings with their default values.

    ``__settings_upgrade_X__``
        List of conversions to upgrade __default_settings__ or current settings
        to new settings/values

    Each entry is structured in the following way::

        ('old_key',
         'new_key',
         [<conversion options>]
         )

    The first entry is the *old key*; if it is different from the *new key* it will be removed.

    The second entry is the *new key*; we will add it to the config. If this is just an empty string,
    we just remove the old key.

    The last entry is a list containing two-pair tuples.
        - If the list is empty, no conversion is made.
        - If the first value is callable i.e. a function, the function will be called with the old
            setting's value.
            Example of conversion old_key to new_key value:
                Old key: 'advanced/data path': "string"
                New key: 'advanced/data path': pathlib object
            Example conversion setup:
                ('advanced/data path',
                 'advanced/data path',
                 [ (lambda p: Path(p) if p is not None else None, None) ]
                )

        - Otherwise each pair describes how to convert the old setting's value.
            NOTE: There must be an entry for every possible value of 'old_key' in order to convert:
                [ (new_value, old_value), (...) ]

    Settings upgrades example - X == upgrade version.
    Only add keys that are already in __default_settings__ from a previous version.

    __setting_upgrade_X__ = [
        # Delete old_key, no longer needed
        ('old_key', None, []),
        ('old_key', '', []),

        # Rename old_key to new_key using old value
        ('old_key', 'new_key', []),

        # Rename old_key to new_key using [callable] to convert
        ('old_key', 'new_key', [ (callable[, options]) ]),
        ('old_key', 'new_key', [ (lambda b: True if BOOLEAN else False) ]

        # Rename old_key to new_key using direct value conversion (boolean example)
        # WARNING: There must be an entry for EVERY value that old_key may have
        ('old_key', 'new_key', [ (<new True value or variable to use>, True),
                                 (<new False value or variable to use>, False) ])
        ]
    """
    __default_settings__ = {  # Default settings - typically only used on first run
        # General settings
        'settings/version': 0,
        # Advanced settings
        'advanced/data path': None,
        'advanced/x11 bypass wm': X11_BYPASS_DEFAULT,
        # Interface options
        'interface/localhost only': True,
        # PJLink doesn't specify max - manufacturer's discretion
        'interface/max connections': 1,
        # PJLink specific options
        'pjlink/pin': None,
        # Log settings
        'logroot/log level': logging.INFO,
        'logprogram/log level': logging.INFO,
        'lognetwork/log level': logging.INFO,
        # Tab specific settings here
        'tabcentral/current index': 0
    }

    __file_path__ = ''

    # ============== Class Methods =================
    @staticmethod
    def extend_default_settings(default_values):
        """
        Static method to merge the given ``default_values`` with the ``Settings.__default_settings__``.

        :param default_values: A dict with setting keys and their default values.
        """
        Settings.__default_settings__.update(default_values)

    @staticmethod
    def set_filename(ini_path):
        """
        Sets the complete path to an Ini file to be used by Settings objects.

        Does not affect existing Settings objects.

        :param openlp.core.common.path.Path ini_path: ini file path
        :rtype: None
        """
        Settings.__file_path__ = str(ini_path)

    @staticmethod
    def set_up_default_values():
        """
        This static method is called on start up. It is used to perform any operation on the __default_settings__ dict.
        """
        # Make sure the string is translated (when building the dict the string is not translated because the translate
        # function was not set up as this stage).
        from terminal.common.i18n import UiStrings
        Settings.__default_settings__['settings/program name'] = UiStrings().ProgramName

    # ============== Instance Methods =================
    def __init__(self, *args, **kwargs):
        """
        Constructor which checks if this should be a native settings object, or an INI file.
        """
        if not args and Settings.__file_path__ and Settings.defaultFormat() == Settings.IniFormat:
            log.debug('Initializing settings')
            QtCore.QSettings.__init__(self, Settings.__file_path__, Settings.IniFormat)
        else:
            log.debug('Initializing settings with default args')
            QtCore.QSettings.__init__(self, *args)
        # Add shortcuts here so QKeySequence has a QApplication instance to use.
        Settings.__default_settings__.update({
            'shortcuts/aboutItem': [QtGui.QKeySequence(QtCore.Qt.CTRL + QtCore.Qt.Key_F1)],
            'shortcuts/collapse': [QtGui.QKeySequence(QtCore.Qt.Key_Minus)],
            'shortcuts/delete': [QtGui.QKeySequence(QtGui.QKeySequence.Delete)],
            'shortcuts/down': [QtGui.QKeySequence(QtCore.Qt.Key_Down)],
            'shortcuts/expand': [QtGui.QKeySequence(QtCore.Qt.Key_Plus)],
            'shortcuts/fileNewItem': [QtGui.QKeySequence(QtGui.QKeySequence.New)],
            'shortcuts/fileSaveAsItem': [QtGui.QKeySequence(QtGui.QKeySequence.SaveAs)],
            'shortcuts/fileExitItem': [QtGui.QKeySequence(QtGui.QKeySequence.Quit)],
            'shortcuts/fileSaveItem': [QtGui.QKeySequence(QtGui.QKeySequence.Save)],
            'shortcuts/fileOpenItem': [QtGui.QKeySequence(QtGui.QKeySequence.Open)],
            'shortcuts/userManualItem': [QtGui.QKeySequence(QtGui.QKeySequence.HelpContents)],
            'shortcuts/moveUp': [QtGui.QKeySequence(QtCore.Qt.Key_PageUp)],
            'shortcuts/moveTop': [QtGui.QKeySequence(QtCore.Qt.Key_Home)],
            'shortcuts/modeSetupItem': [],
            'shortcuts/moveBottom': [QtGui.QKeySequence(QtCore.Qt.Key_End)],
            'shortcuts/moveDown': [QtGui.QKeySequence(QtCore.Qt.Key_PageDown)],
            'shortcuts/searchShortcut': [QtGui.QKeySequence(QtGui.QKeySequence.Find)],
            'shortcuts/settingsShortcutsItem': [],
            'shortcuts/settingsImportItem': [],
            'shortcuts/settingsConfigureItem': [QtGui.QKeySequence(QtGui.QKeySequence.Preferences)],
            'shortcuts/shortcutAction_B': [QtGui.QKeySequence(QtCore.Qt.Key_B)],
            'shortcuts/shortcutAction_C': [QtGui.QKeySequence(QtCore.Qt.Key_C)],
            'shortcuts/shortcutAction_E': [QtGui.QKeySequence(QtCore.Qt.Key_E)],
            'shortcuts/shortcutAction_I': [QtGui.QKeySequence(QtCore.Qt.Key_I)],
            'shortcuts/shortcutAction_O': [QtGui.QKeySequence(QtCore.Qt.Key_O)],
            'shortcuts/shortcutAction_P': [QtGui.QKeySequence(QtCore.Qt.Key_P)],
            'shortcuts/shortcutAction_V': [QtGui.QKeySequence(QtCore.Qt.Key_V)],
            'shortcuts/shortcutAction_0': [QtGui.QKeySequence(QtCore.Qt.Key_0)],
            'shortcuts/shortcutAction_1': [QtGui.QKeySequence(QtCore.Qt.Key_1)],
            'shortcuts/shortcutAction_2': [QtGui.QKeySequence(QtCore.Qt.Key_2)],
            'shortcuts/shortcutAction_3': [QtGui.QKeySequence(QtCore.Qt.Key_3)],
            'shortcuts/shortcutAction_4': [QtGui.QKeySequence(QtCore.Qt.Key_4)],
            'shortcuts/shortcutAction_5': [QtGui.QKeySequence(QtCore.Qt.Key_5)],
            'shortcuts/shortcutAction_6': [QtGui.QKeySequence(QtCore.Qt.Key_6)],
            'shortcuts/shortcutAction_7': [QtGui.QKeySequence(QtCore.Qt.Key_7)],
            'shortcuts/shortcutAction_8': [QtGui.QKeySequence(QtCore.Qt.Key_8)],
            'shortcuts/shortcutAction_9': [QtGui.QKeySequence(QtCore.Qt.Key_9)],
            'shortcuts/settingsExportItem': [],
            'shortcuts/up': [QtGui.QKeySequence(QtCore.Qt.Key_Up)],
        })

    def _convert_value(self, setting, default_value):
        """
        This converts the given ``setting`` to the type of the given ``default_value``.

        :param setting: The setting to convert. This could be ``true`` for example.Settings()
        :param default_value: Indication the type the setting should be converted to. For example ``True``
        (type is boolean), meaning that we convert the string ``true`` to a python boolean.

        **Note**, this method only converts a few types and might need to be extended if a certain type is missing!
        """
        # Handle 'None' type (empty value) properly.
        if setting is None:
            # An empty string saved to the settings results in a None type being returned.
            # Convert it to empty unicode string.
            if isinstance(default_value, str):
                return ''
            # An empty list saved to the settings results in a None type being returned.
            elif isinstance(default_value, list):
                return []
        elif isinstance(setting, str):
            if '__Path__' in setting:
                return json.loads(setting, cls=PJLinkJsonDecoder)
        # Convert the setting to the correct type.
        if isinstance(default_value, bool):
            if isinstance(setting, bool):
                return setting
            # Sometimes setting is string instead of a boolean.
            return setting == 'true'
        if isinstance(default_value, int):
            return int(setting)
        return setting

    def get_default_value(self, key):
        """
        Get the default value of the given key
        """
        if self.group():
            key = self.group() + '/' + key
        return Settings.__default_settings__[key]

    def upgrade_settings(self):
        """
        This method is only called to clean up the config. It removes old settings and it renames settings.
        """
        current_version = self.value('settings/version')
        if current_version == __version__:
            log.debug('Settings have not changed - returning')
            return
        elif current_version > __version__:
            log.error('Unable to upgrade: settings_version > module_version. Skipiping upgrade')
            return
        else:
            log.debug('Upgrading settings from verison {old} to {new}'.format(old=current_version,
                                                                              new=__version__))
        for version in range(current_version, __version__):
            version += 1
            try:
                upgrade_list = getattr(self, '__setting_upgrade_{version}__'.format(version=version))
            except AttributeError:
                log.error('Unable to upgrade: __setting_upgrade_{version}__ missing'.format(version=version))
                continue
            for old_keys, new_key, rules in upgrade_list:
                if any([not self.contains(old_key) for old_key in old_keys]):
                    log.warning('One of {key} does not exist, skipping key'.format(key=old_keys))
                    continue
                if new_key:
                    # Get the value of the old_key.
                    old_values = [super(Settings, self).value(old_key) for old_key in old_keys]
                    # When we want to convert the value, we have to figure out the default value (because we cannot get
                    # the default value from the central settings dict.
                    if rules:
                        default_values = rules[0][1]
                        if not isinstance(default_values, (list, tuple)):
                            default_values = [default_values]
                        old_values = [self._convert_value(old_value, default_value)
                                      for old_value, default_value in zip(old_values, default_values)]
                    # Iterate over our rules and check what the old_value should be "converted" to.
                    new_value = None
                    for new_rule, old_rule in rules:
                        # If the value matches with the condition (rule), then use the provided value. This is used to
                        # convert values. E. g. an old value 1 results in True, and 0 in False.
                        if callable(new_rule):
                            new_value = new_rule(*old_values)
                        elif old_rule in old_values:
                            new_value = new_rule
                            break
                    self.setValue(new_key, new_value)
                [self.remove(old_key) for old_key in old_keys if old_key != new_key]
            self.setValue('settings/version', version)

    def value(self, key):
        """
        Returns the value for the given ``key``. The returned ``value`` is of the same type as the default value in the
        *Settings.__default_settings__* dict.

        :param str key: The key to return the value from.
        :return: The value stored by the setting.
        """
        # if group() is not empty the group has not been specified together with the key.
        if self.group():
            default_value = Settings.__default_settings__[self.group() + '/' + key]
        else:
            default_value = Settings.__default_settings__[key]
        setting = super(Settings, self).value(key, default_value)
        return self._convert_value(setting, default_value)

    def setValue(self, key, value):
        """
        Reimplement the setValue method to handle Path objects.

        :param str key: The key of the setting to save
        :param value: The value to save
        :rtype: None
        """
        if isinstance(value, Path) or (isinstance(value, list) and value and isinstance(value[0], Path)):
            value = json.dumps(value, cls=PJLinkJsonEncoder)
        super().setValue(key, value)
