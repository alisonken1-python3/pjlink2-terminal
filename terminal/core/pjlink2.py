# -*- coding: utf-8 -*-
# vim: autoindent shiftwidth=4 expandtab textwidth=120 tabstop=4 softtabstop=4

###############################################################################
# PJLink2 Terminal - PJLink terminal emulator                                 #
# --------------------------------------------------------------------------- #
# Copyright (c) 2018 - Ken Roberts                                            #
# --------------------------------------------------------------------------- #
# This program is free software; you can redistribute it and/or modify it     #
# under the terms of the GNU General Public License as published by the Free  #
# Software Foundation; version 2 of the License.                              #
#                                                                             #
# This program is distributed in the hope that it will be useful, but WITHOUT #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for    #
# more details.                                                               #
#                                                                             #
# You should have received a copy of the GNU General Public License along     #
# with this program; if not, write to the Free Software Foundation, Inc., 59  #
# Temple Place, Suite 330, Boston, MA 02111-1307 USA                          #
###############################################################################
"""
The :mod:`terminal.core.pjlink2` module contains all the PJlink2 network routines
"""

from time import time

from PyQt5.QtCore import QObject, QTimer, pyqtSlot, pyqtSignal
from PyQt5.QtNetwork import QHostAddress, QTcpServer, QUdpSocket

from terminal.common import LOG_NETWORK, get_logger
from terminal.common.constants import PJLINK_PORT, status
from terminal.common.settings import Settings

log = get_logger(__name__)
log_net = get_logger(__name__, logtab=LOG_NETWORK)


class BaseObject(object):
    """
    Base common information to keep track of hardware
    """
    """
    Millisecond accuracy, so 1000*60*5 for five minutes

    timer_precision = 1000 (milliseconds)
    timer_seconds = 60 (seconds per minute)
    timer_minutes = 5 (minutes per timer)
    timer_interval = 300000

    TODO: Testing set timer to 30-second intervals for now
    """
    def __init__(self):
        """
        Initialize the object with basic information
        """
        # Time varuables use time.time() for seconds precision.
        self.erst = 0  # Index to ERST_STATUS list
        self.failed = False
        self.model = None
        self.name = None
        self.power = 0  # Index into self.power_progression
        self.power_callback = None
        self.power_cooldown = None  # Setup in actual class
        # Default to just a basic power progression
        self.power_progression = [status.S_OFF, status.S_STANDBY, status.S_ON]
        # time_total = time_total + (time_stop - time_start) (in seconds)
        self.power_warmup = None  # Setup in actual class
        self.power_state = status.S_OFF
        self.power_state_old = status.S_OFF
        self.time_total = 0
        # time_start is when power changes to POWER_STATUS.[WARMUP|ON]
        self.time_start = 0
        # time_stop is when power changes to POWER_STATUS.[COOLDOWN|STANDBY|OFF]
        self.time_stop = 0
        # Timers used are for sequence from power off->on->off
        self.timer = QTimer()
        self.timer_interval = None
        self.timer_start_interval = None
        self.timer_stop_interval = None
        # Callbacks to update display
        self.update_hours = None

    def power_off(self):
        """
        This keeps track of time only - overload to add actual progressions
        """
        self.timer.stop()
        self.time_stop = time()
        self.time_total = self.time_total + (self.time_stop - self.time_start)
        self.time_start = 0
        self.time_stop = 0

    def power_on(self):
        """
        This keeps track of time only - overload to add actual progressions

        # NOTE: Do all setups before calling this method.
        """
        setTimer = self.timer_interval if self.timer_start_interval is None else self.timer_start_interval
        self.timer.setInterval(setTimer)
        self.timer_start()
        self.time_start = time()
        # Ensure we don't have a zero reference ending up with a negative number
        self.time_stop = time()

    def power_step(self):
        """
        This keeps track of time only - overload to add actual progressions
        Update the time_total whenever the timer event is triggered.
        """
        self.time_stop = time()
        self.time_total = self.time_total + (self.time_stop - self.time_start)

    def power_update(self):
        """
        Update the display
        """
        if not self.power_callback:
            log.debug('{name} power_update() called when no callback set'.format(name=self.name))
            return
        log.debug('{name} Updating lamp power display'.format(name=self.name))
        log.debug('{name} Setting label to "{text}"'.format(name=self.name,
                                                            text=self.power_state.short))
        self.power_callback.setText(self.power_state.short)
        self.power_callback.show()


class Lamp(BaseObject):
    """
    Keep track of lamp objects
    """
    default_timer_interval = 1000 * 30

    def __init__(self, name, model):
        log.debug('Initializing {name}'.format(name=name))
        super().__init__()
        self.name = name
        self.model = model
        self.timer_interval = self.__class__.default_timer_interval
        self.timer_start_interval = 1000 * 10  # 10 seconds for testing - use model class for production
        self.timer_stop_interval = 1000 * 10
        self.power_progression = [status.S_OFF,
                                  status.S_STANDBY,
                                  status.S_WARMUP,
                                  status.S_ON,
                                  status.S_COOLDOWN]
        self.timer.timeout.connect(self.power_step)

    def power_on(self):
        """
        Lamp-specific options
        """
        check = self.power_progression[self.power]
        if check is status.S_OFF:
            log.error('{name}.power_on when power state is unpowered'.format(name=self.name))
            return
        if check not in [status.S_OFF, status.S_STANDBY]:
            log.info('{name}.power_on called while already doing something - ignoring'.format(name=self.name))
            return
        log.debug('{name} timer starting'.format(name=self.name))
        super().power_on()
        self.power_state_old = self.power_progression[self.power]
        # Assume (!) we are at status.S_STANDBY
        self.power += 1
        self.power_state = self.power_progression[self.power]
        log.debug('{name} Calling power_update() to update display'.format(name=self.name))
        self.power_update()

    def power_off(self):
        """
        Lamp-specific options
        """
        check = self.power_progression[self.power]
        if check is status.S_OFF:
            log.error('{name}.power_on() when power state is unpowered - ignoring'.format(name=self.name))
            return
        if check is status.S_STANDBY:
            log.info('{name}.power_off() called when already in standby - ignoring'.format(name=self.name))
            return
        if check in [status.S_WARMUP, status.S_COOLDOWN]:
            log.info('{name}.power_off() called while already doing something - ignoring'.format(name=self.name))
            return
        # Assume (!) we're only called when already in status.S_ON at this point
        self.power_status = self.power_progression[self.power]
        self.power += 1
        self.power_status = self.power_progression[self.power]
        # We don't call super().power_off() until after cooldown
        self.timer.setInterval(self.timer_stop_interval)
        log.debug('{name} changing state from {old} to {new}'.foramt(name=self.name,
                                                                     old=self.power_state_old,
                                                                     new=self.power_state))

    def power_step(self):
        """
        Called on timer.timeout() signal
        """
        # Update times
        super().power_step()
        check = self.power_progression[self.power]
        # Now, check for update state
        if check is status.S_ON:
            # Just updating times
            return
        if check is status.S_WARMUP:
            # Upgrade to on and continue
            self.power += 1
            self.timer.setInterval(self.timer_interval)
        elif check is status.S_COOLDOWN:
            # ok - finished cooldown, finish power off step
            self.timer.stop()
            self.power = 1
        log.debug('{name} changing state from {old} to {new}'.foramt(name=self.name,
                                                                     old=self.power_state_old,
                                                                     new=self.power_state))


class Filter(BaseObject):
    """
    Keep track of filter objects
    """
    def __init__(self, name, model):
        log.debug('Initializing {name}'.format(name=name))
        super().__init__()
        self.model = model
        self.name = name

    def power_off(self):
        """ WIP """
        pass

    def power_on(self):
        """ WIP """
        pass

    def power_step(self):
        """ WIP """
        pass


class Pjl2Udp(QUdpSocket):
    """
    Class to send a UDP datagram for PJLink2 status updates
    """
    sendStatus = pyqtSignal(QHostAddress, str)

    def __init__(self, adx, parent=None, **kwargs):
        """
        Initialization setup.

        :param adx: QHostAddress or string IP of local interface to use
        :param parent: Used by PyQt5
        """
        super().__init__(parent=parent, **kwargs)
        if type(adx) is type(QHostAddress):
            self.bind(adx, 0, self.ReuseAddressHint | self.ShareAddress)
        else:
            self.bind(QHostAddress(adx), 0, self.ReuseAddressHint | self.ShareAddress)

    @pyqtSlot(QHostAddress, str)
    def send_packet(self, host, data):
        """
        Send <data> to <host> on <port>

        :param host: QHostAddress recipient address
        :param data: Packet to send
        """
        if host.protocol() != self.localAddress().protocol():
            # Not in my network class
            log.debug('{dest} address not accessible by {host}'.format(dest=host.toString(),
                                                                       host=self.localAddress.toString()))
            return
        log.debug('Sending "{packet}" to {dest}'.format(packet=data,
                                                        dest=host.toString()))


class Pjl2Server(QTcpServer):
    """
    Class to manage communications with controller
    """
    power_on_transition_default = 1000 * 10  # 10 seconds for now, think about production later

    def __init__(self, adx, ip='4', port=PJLINK_PORT, parent=None, **kwargs):
        """
        Initialize server

        :param adx: QNetworkInterface instance
        :param parent: Used by PyQt
        """
        super().__init__(parent=parent, **kwargs)
        self.setMaxPendingConnections(Settings().value('interface/max connections'))
        if ip not in ['4', '6']:
            log.warning('Invalid TCIP protocol - defaulting to IPv4')
            ipchk = 'ip4'
        elif ip == '6':
            ipchk = 'ip6'
        else:
            ipchk = 'ip4'
        self.my_address = adx[ipchk]
        self.name = '{ip}:{port}'.format(ip=self.my_address.toString(), port=port)
        log.debug('Setting name to "{name}"'.format(name=self.name))
        self.my_port = port
        # Hardware aspects of terminal
        self.clss = 2  # Default to latest version
        self.filters_max = 1
        self.filters = []
        self.lamps_max = 1
        self.lamps = []
        self.mic_avail = True  # Microphone installed
        self.mic_mute = False  # AVMT
        self.mic_vol = 0  # Although PJLink doesn't monitor actual volume - future enhancement possible
        self.power = 0  # Index to POWER_STATUS
        self.power_callback = None  # This should be a QSlider
        self.power_on_transition = self.__class__.power_on_transition_default
        self.power_progression = [status.S_OFF, status.S_INITIALIZE, status.S_STANDBY, status.S_ON]
        self.power_state = status.S_OFF
        self.power_state_callback = None  # This should be a QLabel
        self.power_timer = QTimer()
        self.power_timer.setInterval(self.power_on_transition)
        self.power_timer.setSingleShot(True)  # Only used for initialize->on
        self.speaker_avail = True
        self.speaker_mute = False  # AVMT
        self.speaker_vol = 0  # Same as mic_vol
        self.shutter = False  # Shutter open/unmuted (False) or closed/muted (True)
        # Connect signals
        self.power_timer.timeout.connect(self.power_apply)

    def add_filter(self, name, model=None):
        """
        Add a filter
        """
        if len(self.filters) >= self.filters_max:
            log.warn('Attempting to add a new filter when already full')
            return
        item = [i.name for i in self.filters if i.name != name]
        if item:
            log.error('{name} already in list - ignoring'.format(name=item.name))
            return
        log.debug('Adding new {name} model {model}'.format(name=name, model=model))
        self.filters.append(Filter(name=name, model=model))

    def add_lamp(self, name, model=None):
        """
        Add a lamp
        """
        if len(self.lamps) >= self.lamps_max:
            log.warn('Attempting to add a new lamp when already full')
            return
        item = [i.name for i in self.lamps if i.name != name]
        if item in self.lamps:
            log.error('{name} already in list - ignoring'.format(name=item.name))
            return
        log.debug('Adding new {name} model {model}'.format(name=name, model=model))
        self.lamps.append(Lamp(name=name, model=model))

    def power_apply(self):
        """
        Call this to simulate device being plugged in to a power source
        """
        if self.power_state is status.S_ON:
            log.warning('{name}.power_apply called when already on - ignoring'.format(name=self.name))
            return
        log.debug('{name} Checking power state {state}'.format(name=self.name,
                                                               state = self.power_state.short))
        if self.power_state is status.S_OFF:
            # Single shot timer to change to power on
            self.power = self.power_progression.index(self.power_state) + 1
            self.power_state = self.power_progression[self.power]
            log.debug('{name} Setting power to {check}'.format(name=self.name,
                                                               check=self.power_state.short))
            self.power_timer.start()
            return self.update_power_state_display()

        if self.power_state is status.S_INITIALIZE:
            self.power = self.power_progression.index(self.power_state) + 1
            self.power_state = self.power_progression[self.power]
            log.debug('{name} Setting power to {state}'.format(name=self.name,
                                                               state = self.power_state.short))
            self.update_power_state_display()

        if self.lamps:
            log.info('Setting lamps to {status}'.format(status=status.S_STANDBY.short))
            for item in self.lamps:
                item.power_state_old = item.power_state
                item.power = item.power_progression.index(status.S_OFF) + 1
                item.power_state = item.power_progression[item.power]
                item.power_update()
        if self.filters:
            log.info('Setting filters to {status}'.format(status=status.S_STANDBY.short))
            for item in self.filters:
                item.power_state_old = item.power_state
                item.power = item.power_progression.index(status.S_OFF) + 1
                item.power_state = item.power_progression[item.power]
                item.power_update()

    def power_off(self):
        """
        Turn off the power to this device.
        NOTE: See power_on() for caveat
        """
        if self.power_progression[self.power] is status.S_OFF:
            log.warning('Attempting to power_on() before appy_power() - returning')
            return
        # Items should already handle case of multiple calls to power_on
        for item in self.lamps:
            item.power_off()
            item.update_power()
        for item in self.filters:
            item.power_off()
            item.update_power()

    def power_on(self):
        """
        Turn on the power to this device.

        NOTE: This is for changing power state from standby->on
              Use apply_power() to simulate terminal being plugged into a power source before
              calling this function.
        """
        if self.power_progression[self.power] is status.S_OFF:
            log.warning('Attempting to power_on() before appy_power() - returning')
            return
        # Items should already handle case of multiple calls to power_on
        for item in self.lamps:
            item.power_on()
            item.update_power()
        for item in self.filters:
            item.power_on()
            item.update_power()

    def power_remove(self):
        """
        Call this method when the device is unplugged/power is removed
        """
        log.info('Removing power to lamps and filters')
        for item in self.lamps:
            item.power = 0
            item.power_state = item.power_progression[item.power]
            item.power_state_old = item.power_state
            item.power_update()
        for item in self.filters:
            item.power = 0
            item.power_state = item.power_progression[item.power]
            item.power_update()
            item.power_state_old = item.power_state
        self.update_power_state_display()

    def update_power_state_display(self):
        """
        Update the terminal status display
        """
        if not self.power_state_callback:
            log.error('{name} No power state callback - skipping'.format(name=self.name))
            return
        log.debug('{name} Updating power status display to {status}'.format(name=self.name,
                                                                            status=self.power_state.short))
        self.power_state_callback.setText(self.power_state.short)
