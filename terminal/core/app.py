# -*- coding: utf-8 -*-
# vim: autoindent shiftwidth=4 expandtab textwidth=120 tabstop=4 softtabstop=4

###############################################################################
# PJLink2 Terminal - PJLink terminal emulator                                 #
# --------------------------------------------------------------------------- #
# Copyright (c) 2018 - Ken Roberts                                            #
# --------------------------------------------------------------------------- #
# This program is free software; you can redistribute it and/or modify it     #
# under the terms of the GNU General Public License as published by the Free  #
# Software Foundation; version 2 of the License.                              #
#                                                                             #
# This program is distributed in the hope that it will be useful, but WITHOUT #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for    #
# more details.                                                               #
#                                                                             #
# You should have received a copy of the GNU General Public License along     #
# with this program; if not, write to the Free Software Foundation, Inc., 59  #
# Temple Place, Suite 330, Boston, MA 02111-1307 USA                          #
###############################################################################
"""
The :mod:`app` module contains necessary functionality for starting the program
"""
import argparse
import logging
import sys

from PyQt5 import QtWidgets

from terminal.common import IS_WIN, IS_MACOSX, file_formatter
from terminal.common.applocation import AppLocation
from terminal.common.path import create_paths
from terminal.common.settings import Settings
from terminal.ui.display_ui import MainWindow


__all__ = ['Pjl2Terminal', 'main']

log = logging.getLogger()


class Pjl2Terminal(QtWidgets.QApplication):
    """
    Main program class
    """
    args = []

    def exec(self):
        """
        Override exec method to allow the shared memory to be released on exit
        """
        log.debug('Executing Pjl2Terminal.exec()')
        return QtWidgets.QApplication.exec()

    def run(self, args):
        """
        Run the OpenLP application.

        :param args: Some Args
        """
        log.debug('Executing Pjl2Terminal.run()')
        self.terminals = []
        self.current_terminal = None

        log.debug('Creating MainWindow()')
        self.main_window = MainWindow()
        self.main_window.show()
        log.debug('Processing events')
        self.processEvents()
        return self.exec()


def main(args=None):
    """
    Entry point for starting the program

    :param args: Some args
    """
    # Set up command line options.
    parser = argparse.ArgumentParser(prog='pjlink2.py')
    parser.add_argument('-l', '--log-level', dest='loglevel', default='info', metavar='LEVEL',
                        help='Set logging to LEVEL. Valid values are "debug", "info", "warning".')
    parser.add_argument('-p', '--pgm-log-level', dest='pgmloglevel', default='info', metavar='PGMLEVEL',
                        help='Set program logging to PGMLEVEL. Valid values are "debug", "info", "warning".')
    parser.add_argument('-n, ''--net-log-level', dest='netloglevel', default='info', metavar='NETLEVEL',
                        help='Set network logging to NETLEVEL. Valid values are "debug", "info", "warning".')
    parser.add_argument('rargs', nargs='?', default=[])
    # Parse command line options and deal with them. Use args supplied pragmatically if possible.
    args = parser.parse_args(args) if args else parser.parse_args()
    qt_args = []
    # Throw the rest of the arguments at Qt, just in case.
    qt_args.extend(args.rargs)
    # Set the WM_CLASS property in X11
    if not IS_WIN and not IS_MACOSX:
        qt_args.append('PJLink2')

    # Now create and actually run the application.
    application = Pjl2Terminal(qt_args)
    application.setOrganizationName('PJLink2')
    application.setApplicationName('PJLink2')
    # Upgrade settings.
    settings = Settings()
    # Check for upgrades to Settings()
    settings.upgrade_settings()
    # Setup basic logging
    log_path = AppLocation.get_directory(AppLocation.CacheDir)
    create_paths(log_path, do_not_log=True)
    file_path = log_path / 'pjlink2.log'
    logfile = logging.FileHandler(str(file_path), 'w', encoding='UTF-8')
    logfile.setFormatter(file_formatter)
    log.addHandler(logfile)
    # Update settings
    # HighDPI options
    # application.setAttribute(QtCore.Qt.AA_UseHighDpiPixmaps, True)
    # application.setAttribute(QtCore.Qt.AA_DontCreateNativeWidgetSiblings, True)
    level = log.getEffectiveLevel()
    settings.setValue('logroot/log level', level)
    log.info('Setting file log level to {level}'.format(level=logging.getLevelName(level)))
    if args and args.loglevel.lower() in ['d', 'debug']:
        Settings().setValue('logroot/log level', logging.DEBUG)
    elif args and args.loglevel.lower() in ['w', 'warning']:
        Settings().setValue('logroot/log level', logging.WARNING)

    log.setLevel(Settings().value('logroot/log level'))
    if log.isEnabledFor(logging.DEBUG):
        print('Logging to: {name}'.format(name=file_path))

    # Program tab logging
    if args and args.pgmloglevel.lower() in ['d', 'debug']:
        log.info('Setting program tab log level to {level}'.format(level=logging.DEBUG))
        settings.setValue('logprogram/log level', logging.DEBUG)
    elif args and args.pgmloglevel.lower() in ['w', 'warning']:
        log.info('Setting program tab log level to {level}'.format(level=logging.WARNING))
        settings.setValue('logprogram/log level', logging.WARNING)
    # Network tab logging
    if args and args.netloglevel.lower() in ['d', 'debug']:
        log.info('Setting network tab log level to {level}'.format(level=logging.DEBUG))
        settings.setValue('lognetwork/log level', logging.DEBUG)
    elif args and args.netloglevel.lower() in ['w', 'warning']:
        log.info('Setting network tab log level to {level}'.format(level=logging.WARNING))
        settings.setValue('lognetwork/log level', logging.WARNING)

    sys.exit(application.run(qt_args))
