# Resources

This directory contains the resources used.

* resources/forms  - Qt ui files used to create interface(s)
* resources/i18n   - Internationalization files
* resources/images - Image/icon files used in program interface
