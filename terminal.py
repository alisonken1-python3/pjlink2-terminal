#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: autoindent shiftwidth=4 expandtab textwidth=120 tabstop=4 softtabstop=4

###############################################################################
# PJLink2 Terminal - PJLink terminal emulator                                 #
# --------------------------------------------------------------------------- #
# Copyright (c) 2018 - Ken Roberts                                            #
# --------------------------------------------------------------------------- #
# This program is free software; you can redistribute it and/or modify it     #
# under the terms of the GNU General Public License as published by the Free  #
# Software Foundation; version 2 of the License.                              #
#                                                                             #
# This program is distributed in the hope that it will be useful, but WITHOUT #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for    #
# more details.                                                               #
#                                                                             #
# You should have received a copy of the GNU General Public License along     #
# with this program; if not, write to the Free Software Foundation, Inc., 59  #
# Temple Place, Suite 330, Boston, MA 02111-1307 USA                          #
###############################################################################
"""
The main entry point for PJLink2 Termainal
"""
import multiprocessing
import sys

from terminal.core.app import main
from terminal.common import IS_MACOSX, IS_WIN

if __name__ == '__main__':
    """
    Instantiate and run the application.
    """
    if IS_WIN:
        # Add support for using multiprocessing from frozen Windows executable (built using PyInstaller),
        # see https://docs.python.org/3/library/multiprocessing.html#multiprocessing.freeze_support
        multiprocessing.freeze_support()
    elif IS_MACOSX:
        # Mac OS X passes arguments like '-psn_XXXX' to the application. This argument is actually a process serial number.
        # However, this may cause a conflict with other arguments. Since we do not use this argument we can delete it
        # to avoid any potential conflicts.
        sys.argv = [x for x in sys.argv if not x.startswith('-psn')]
    main()
